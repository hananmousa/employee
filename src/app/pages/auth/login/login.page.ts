import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  logindata:any={
  email:undefined,
  password:undefined
  }
  loginerror:any={
    email:"",
    password:""
    }
 
    EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    isLoading: boolean = false;
  constructor( public toastController:ToastController, private router: Router, private navCtrl:NavController,public ngFireAuth:AngularFireAuth) {
  }

  ngOnInit() {
  }
  async login(){
    let error=false
    this.loginerror.email=''
    this.loginerror.password=''
    if (!this.logindata.email) {
      this.loginerror.email = "This Field is required"
      error=true
    }
    if(!this.EMAIL_REGEXP.test(this.logindata.email))
    {
     this.loginerror.email = "Please enter correct email"
 
    }
    if (!this.logindata.password) {
      this.loginerror.password = "This Field is required"
      error=true
    }
    if(!error)
    {
    this.ngFireAuth.signInWithEmailAndPassword(this.logindata.email, this.logindata.password).then(async (data)=>{
    console.log(data)
      let UserInfo={
      'email':this.logindata.email,
      'password':this.logindata.password
     }
      localStorage.setItem('UserInfo',JSON.stringify(UserInfo))
      this.navCtrl.navigateForward('/home')

    }) .catch(async e => {
      const toast =  this.toastController.create({
        message: e.message,
        duration: 1000,
        position: 'bottom'
      });
  
      (await toast).present();
    
  });
  
  }
  }
  register()
  {
    this.navCtrl.navigateForward(['/register'])
  }
}
