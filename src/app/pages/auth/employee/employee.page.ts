import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.page.html',
  styleUrls: ['./employee.page.scss'],
})
export class EmployeePage implements OnInit {
  employee_data:any={}
  constructor(public router: Router,public navCtrl:NavController) { 
   
  }

  ngOnInit() {
   
      this.employee_data=this.router.getCurrentNavigation()?.extras?.state?.['employee_data']
  }
edit()
{
  let navigationExtras: NavigationExtras = {
    state: {
      'employee_data':this.employee_data,
      'page_name':"Edit"
    }
  };
  this.navCtrl.navigateForward(['/addemployee'],navigationExtras)
}
goToBack()
{
  this.navCtrl.back()
}
}
