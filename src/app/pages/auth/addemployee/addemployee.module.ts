import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddemployeePageRoutingModule } from './addemployee-routing.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

import { AddemployeePage } from './addemployee.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddemployeePageRoutingModule,
    RoundProgressModule
  ],
  declarations: [AddemployeePage]
})
export class AddemployeePageModule {}
