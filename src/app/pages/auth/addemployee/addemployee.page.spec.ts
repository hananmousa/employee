import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddemployeePage } from './addemployee.page';

describe('AddemployeePage', () => {
  let component: AddemployeePage;
  let fixture: ComponentFixture<AddemployeePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AddemployeePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
