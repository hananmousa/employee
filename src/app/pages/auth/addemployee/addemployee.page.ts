import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { Camera, CameraResultType } from '@capacitor/camera';

@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.page.html',
  styleUrls: ['./addemployee.page.scss'],
})
export class AddemployeePage implements OnInit {
  page_name:any
  employeedata: any = {
    name: undefined,
    image: undefined,
    job_title: undefined,
    address: false,
    mobile: undefined,
    ID: false,
    email:undefined,
    Dateofbirth:undefined,
  };
  employeeError:any={
    name: "",
    image: "",
    job_title: "",
    address: "",
    mobile: "",
    ID: "",
    email:"",
    Dateofbirth:""
  }
  EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  employees:any=[]
  constructor(private router:Router,private navCtrl:NavController,private toastController:ToastController) { }

  ngOnInit() {
    if (this.router.getCurrentNavigation()?.extras.state) {

    this.page_name=this.router.getCurrentNavigation()?.extras?.state?.['page_name']
    if(this.page_name=="Edit")
    {
    this.employeedata=this.router.getCurrentNavigation()?.extras?.state?.['employee_data']
    
    }
    else{
      this.employees=this.router.getCurrentNavigation()?.extras?.state?.['employee_data']
      console.log(JSON.stringify(this.employees))
    }
    }
  }
 async addemployee()
 {
  let error = false;
  this.employeedata.Dateofbirth=this.employeedata.Dateofbirth.split('T')[0];
  if (!this.employeedata.image) {
    this.employeeError.image = "This Field is required"
    error=true
  }
    if (!this.employeedata.name) {
      this.employeeError.name = "This Field is required"
      error=true
    }
    if (!this.employeedata.job_title) {
      this.employeeError.job_title = "This Field is required"
      error=true
    }
    if (!this.employeedata.mobile) {
      this.employeeError.mobile = "This Field is required"
      error=true
    }
    
    if (!this.employeedata.email) {
      this.employeeError.email = "This Field is required"
      error=true
    }
    if(! this.EMAIL_REGEXP.test(this.employeedata.email))
    {
     this.employeeError.email = "Please enter correct email"
  
    }
    if (!this.employeedata.address) {
      this.employeeError.address = "This Field is required"
      error=true
    }
    if (!this.employeedata.ID) {
      this.employeeError.ID = "This Field is required"
      error=true
    }
    if (!this.employeedata.Dateofbirth) {
      this.employeeError.Dateofbirth = "This Field is required"
      error=true
    }
    if (!error) {

      this.employees.push(this.employeedata)
      localStorage.setItem('employeedata',this.employees)

      this.navCtrl.navigateForward(['/home'])
      const toast =  this.toastController.create({
        message: "employee add successfully",
        duration: 1000,
        position: 'bottom'
      });
  
      (await toast).present();

    }
 }
 async editemployee()
 {
  let navigationExtras: NavigationExtras = {
    state: {
      'employee_data':this.employeedata,
      'page_name':'Edit'
    }
  };
  this.navCtrl.navigateForward(['/home'],navigationExtras)

  const toast =  this.toastController.create({
    message: "your data has been modified successfully",
    duration: 1000,
    position: 'bottom'
  });

  (await toast).present();

 }

 async takePicture() {
  const image= await Camera.getPhoto({
    quality: 90,
    allowEditing: true,
    resultType: CameraResultType.Uri
  });
  this.employeedata.image=image.webPath
}

goToBack()
{
  this.navCtrl.back()
}
}
