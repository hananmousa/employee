import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { elementAt } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  employees:any=[
    {
      image:"assets/images/employee.jpg",
      name:"mohamed ahmed",
      job_title:"mobile developer",
      address:"new cairo",
      mobile:"01236546457",
      ID:"2569",
      Dateofbirth:"1992-10-10",
      email:"email@gmail.com"
    },
    {
      image:"assets/images/employee.jpg",
      name:"ali ahmed",
      job_title:"backend developer",
      address:"new cairo",
      mobile:"01236546457",
      ID:"2569",
      Dateofbirth:"1992-10-10",
      email:"email@gmail.com"
    },
    {
      image:"assets/images/employee.jpg",
      name:"rayan gamal",
      job_title:"frontend developer",
      address:"new cairo",
      mobile:"01236546457",
      ID:"2569",
      Dateofbirth:"1992-10-10",
      email:"email@gmail.com"
    },
    {
      image:"assets/images/employee.jpg",
      name:"mai mostafa",
      job_title:"ui/ux designer",
      address:"new cairo",
      mobile:"01236546457",
      ID:"2569",
      Dateofbirth:"1992-10-10",
      email:"email@gmail.com"
    },
  ]
  page_name:any
  filter_list:any=this.employees
  employeesdata:any={}
  constructor(private navCtrl:NavController,private router:Router) { 
    this.page_name=this.router.getCurrentNavigation()?.extras.state?.['page_name']
    this.employeesdata=this.router.getCurrentNavigation()?.extras.state?.['employee_data']
    

  }

  ngOnInit() {

  }
  ionViewWillEnter()
  {
    this.page_name=this.router.getCurrentNavigation()?.extras.state?.['page_name']
    this.employeesdata=this.router.getCurrentNavigation()?.extras.state?.['employee_data']
  
  if(this.page_name=="Edit")
    {
      this.employees.forEach((element:any) => {
        if(element.ID==this.router.getCurrentNavigation()?.extras?.state?.['employee_data'].ID)
        {
          element=this.router.getCurrentNavigation()?.extras?.state?.['employee_data']
          localStorage.setItem('employeedata',this.employees)

        }
      });
    }
    
  }
  handleInput(evt:any){
   
    this.employees=[]
    this.filter_list.forEach((element:any) => {
      if(element.name.includes(evt.srcElement.value) || element.job_title.includes(evt.srcElement.value))
      { 

        this.employees.push(element)
      }
    
    });
    
   
  }
  onCancel(evt:any)
  {
    this.employees=this.filter_list
   
  }
  addemployee()
  {
    let navigationExtras: NavigationExtras = {
      state: {
        'page_name':"add",
        'employee_data':this.employees
      }
    };
    this.navCtrl.navigateForward(['/addemployee'],navigationExtras)
  }
  details(item:any)
  {
    let navigationExtras: NavigationExtras = {
      state: {
        'employee_data':item,
      }
    };
    this.navCtrl.navigateForward(['/employee'],navigationExtras)
  }
}
