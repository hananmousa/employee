import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {  NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerdata:any={
    password: undefined,
    email: undefined,
  };
  registererror:any={
    password: "",
    email: "",
  };
  isLoading: boolean = false;
  EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(public navCtrl:NavController,public ngFireAuth:AngularFireAuth,public toastController:ToastController) { }

  ngOnInit() {
  }
  async register()
{
  let error=false
  this.registererror.email=''
  this.registererror.password=''
  if (!this.registerdata.email) {
    this.registererror.email = "This Field is required"
    error=true
  }
  if(! this.EMAIL_REGEXP.test(this.registerdata.email))
  {
   this.registererror.email = "Please enter correct email"

  }
  if (!this.registerdata.password) {
    this.registererror.password = "This Field is required"
    error=true
  }
  if(!error)
  {
 
  this.ngFireAuth.createUserWithEmailAndPassword(this.registerdata.email, this.registerdata.password).then(async (data)=>{
    
    localStorage.setItem('UserInfo',JSON.stringify(this.registerdata))
    this.navCtrl.navigateForward('/home')
      const toast =  this.toastController.create({
        message: 'Your account created successfully',
        duration: 1000,
        position: 'bottom'
      });
  
      (await toast).present();
    
  }).catch(async e => {
    const toast =  this.toastController.create({
      message: e.message,
      duration: 1000,
      position: 'bottom'
    });

    (await toast).present();
  
});
}

}
}
