import { Component } from '@angular/core';
import { initializeApp } from '@angular/fire/app';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(public router:Router,public navCtrl:NavController,public ngFireAuth:AngularFireAuth) {
    this.initializeApp();

  }
  initializeApp()
  {
  if(!(localStorage.getItem("UserInfo")==null))
  {
    
     this.navCtrl.navigateForward(['/home'])
  }
  else{
    this.navCtrl.navigateForward(['/login'])
  }
  }

logout()
{
  return this.ngFireAuth.signOut().then(() => {
    localStorage.removeItem('UserInfo');
    this.router.navigate(['/login']);
  });
}
}
