import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
 
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule),
  
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/auth/home/home.module').then( m => m.HomePageModule),
    data: {
      preload: true
    },
  },
  {
    path: 'employee',
    loadChildren: () => import('./pages/auth/employee/employee.module').then( m => m.EmployeePageModule),
    data: {
      preload: true
    },
  },
  {
    path: 'addemployee',
    loadChildren: () => import('./pages/auth/addemployee/addemployee.module').then( m => m.AddemployeePageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
