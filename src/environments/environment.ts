// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyBVFfYjxq10tBZyFp5XjezFySo6fxu1FTk",
    authDomain: "employee-f2dc1.firebaseapp.com",
    projectId: "employee-f2dc1",
    storageBucket: "employee-f2dc1.appspot.com",
    messagingSenderId: "198117058395",
    appId: "1:198117058395:web:982beef6737f2038149c7e",
    measurementId: "G-G03347ZLD1"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
